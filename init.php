<?php
session_start();

require_once 'classes/user.php';
require_once 'classes/client.php';
require_once 'classes/manager.php';
require_once 'classes/admin.php';

$users = [
    0 => ['email'=>'vasia@gmail.com', 'name' => 'Василий', 'surname' => 'Лоханкин', 'login' => 'Vasisualiy', 'password' => '12345', 'lang' => 'ru', 'role' => 'admin'],
    1 => ['email'=>'cukerman@gmail.com', 'name' => 'Афанасий', 'surname' => 'Цукерберг', 'login' => 'Afanasiy', 'password' => '54321', 'lang' => 'en', 'role' => 'client'],
    2 => ['email'=>'Petro@gmail.com', 'login' => 'Petro', 'name' => 'Петр', 'surname' => 'Инкогнито', 'password' => 'EkUC', 'lang' => 'ua', 'role' => 'manager'],
    3 => ['email'=>'Pedro222@gmail.com', 'login' => 'Pedrolus', 'name' => 'Педро', 'surname' => 'Миланов', 'password' => 'Cogi', 'lang' => 'it', 'role' => 'client'],
    4 => ['email'=>'Alexxx@gmail.com', 'login' => 'Sasha', 'name' => 'Александр', 'surname' => 'Александров', 'password' => 'c2usat', 'role' => 'manager'],
];



?>
