<?php
require_once 'init.php';

class User
{
    // объявление свойства
    protected $login;
    protected $password;
    protected $name;
    protected $surname = 'Инкогнито';
    protected $role = 'client';
    protected $lang = 'unknown';
    protected $id;
    protected $email;
    private $users;

    function __construct()
    {
        if (isset($_SESSION["user"]))     //если массив есть - достаем из него все данные и раскладываем по переменным
        {
            $this->login = $_SESSION["user"]["login"];
            $this->password = $_SESSION["user"]["password"];
            $this->name = $_SESSION["user"]["name"];
            $this->surname = $_SESSION["user"]["surname"];
            $this->role = $_SESSION["user"]["role"];
            $this->lang = $_SESSION["user"]["lang"];
            $this->id = $_SESSION["user"]["id"];
            $this->email = $_SESSION["user"]["email"];
            $this->users = $GLOBALS['users'];
        }
    }

    /**
     * @param $email
     * @param $pass
     * Метод проверяет пароль на правильность, и в случае, если пароль правильный, ложит в $_SESSION["user"]["id"] айди залогиненного пользователя
     */
    public function autentificated()
    {
        if (isset($_POST["email"]) and isset($_POST["pass"])) {      //проверяем чтоб данные были, иначе выдает ошибку
            $this->email = $_POST["email"];
            $this->password = $_POST["pass"];

            foreach ($this->users as $key => $value) {
                if (($value['email'] == $this->email) and ($value['password'] == $this->password)) {
                    $this->id = $key;
                    $this->name = $value['name'];
                    $this->surname = $value['surname'];
                    $this->role = $value['role'];
                    $this->lang = $value['lang'];
                    $this->login = $value['login'];
                    break;
                }
            }
            if (!isset($this->id)) {
                echo "Ошибка: Пользователь с такими данными не найден";
            }
        } else {
            echo "Заполните, пожалуйста, все поля";
        }
    }

    /**
     * Выводит что-то вроде “Здравствуйте, менеджер Иван Иванов. Вы можете на сайте изменять, удалять и создавать клиентов”
     */
    public function welcome()
    {
        switch ($this->lang) {
            case 'ru':
                $hi = "Здравствуйте";
                $role_lang = "клиент";
                $role_comment = "Вы можете на сайте просматривать информацию, доступную пользователям";
                if ($this->role == 'manager') {
                    $role_lang = "менеджер";
                    $role_comment = "Вы можете на сайте изменять, удалять и создавать клиентов";
                }
                if ($this->role == 'admin') {
                    $role_lang = "админ";
                    $role_comment = "Вы можете на сайте делать всё";
                }
                break;
            case 'en':
                $hi = "Hello";
                $role_lang = "client";
                $role_comment = "You can view the information available to users on the site";
                if ($this->role == 'manager') {
                    $role_lang = "manager";
                    $role_comment = "You can change, delete and create clients on the site";
                }
                if ($this->role == 'admin') {
                    $role_lang = "admin";
                    $role_comment = "You can do everything on the site";
                }
                break;
            case 'ua':
                $hi = "Здоровенькі були";
                $role_lang = "клієнт";
                $role_comment = "Ви можете на сайті переглядати інформацію, доступну користувачам";
                if ($this->role == 'manager') {
                    $role_lang = "менеджер";
                    $role_comment = "Ви можете на сайті змінювати, видаляти і створювати клієнтів";
                }
                if ($this->role == 'admin') {
                    $role_lang = "адмін";
                    $role_comment = "Ви можете на сайті робити все";
                }
                break;
            case 'it':
                $hi = "Ciao";
                $role_lang = "cliente";
                $role_comment = "È possibile visualizzare le informazioni disponibili per gli utenti sul sito";
                if ($this->role == 'manager') {
                    $role_lang = "manager";
                    $role_comment = "Puoi modificare, eliminare e creare client sul sito";
                }
                if ($this->role == 'admin') {
                    $role_lang = "admin";
                    $role_comment = "Puoi fare tutto sul sito";
                }
                break;
        }
        echo $hi . ', ' . $role_lang . ' ' . $this->name . ' ' . $this->surname . '! ' . $role_comment;  //приветствие, согласно роли и языку
    }

    public function creare_un_erede()         //создать наследника
    {
        switch ($this->role) {
            case 'client':
                $client = new Client();
                break;
            case 'manager':
                $manager = new Manager();
                break;
            case 'admin':
                $admin = new Admin();
                break;
        }
    }


    function __destruct()        //Сохраняем все данные в сессию
    {
        $_SESSION["user"]["login"] = $this->login;
        $_SESSION["user"]["password"] = $this->password;
        $_SESSION["user"]["name"] = $this->name;
        $_SESSION["user"]["surname"] = $this->surname;
        $_SESSION["user"]["role"] = $this->role;
        $_SESSION["user"]["lang"] = $this->lang;
        $_SESSION["user"]["id"] = $this->id;
        $_SESSION["user"]["email"] = $this->email;
    }
}